import javafx.application.Application
import xosmig.matching.MainForm

fun main(args: Array<String>) {
    Application.launch(MainForm::class.java, *args)
}
