package xosmig.matching

import java.util.*
import kotlin.collections.ArrayList
import xosmig.matching.Game.ClickResult.*

class Game(val size: Int) {

    private val grid = run {
        val random = Random()
        val valueCnt = size * size / 2
        val used = Array(valueCnt) { 0 }

        Array(size) {
            Array(size) {
                var value: Int
                do {
                    value = random.nextInt(valueCnt)
                } while (used[value] == 2)
                used[value] += 1
                Cell(value)
            }
        }
    }

    private var openedPoint: Point? = null

    private var cellsRemainder = size * size

    init {
        if (size % 2 != 0) {
            throw IllegalArgumentException("Even size expected")
        }
    }

    private fun get(point: Point) = grid[point.lineN][point.columnN]

    private fun MutableList<ClickResult>.openCell(point: Point) {
        this.add(OpenCell(point, get(point).value))
        get(point).isOpened = true
    }

    private fun MutableList<ClickResult>.closeCell(point: Point) {
        this.add(CloseCell(point))
        get(point).isOpened = false
    }

    private fun MutableList<ClickResult>.blockCell(point: Point) {
        this.add(BlockCell(point))
        cellsRemainder -= 1
    }

    @Synchronized
    fun click(point: Point): List<ClickResult> {
        if (get(point).isOpened) {
            return emptyList()
        }

        val res = ArrayList<ClickResult>()
        res.openCell(point)

        val openedPoint = this.openedPoint  // to allow smartcast
        if (openedPoint != null) {
            if (get(openedPoint).value == get(point).value) {
                res.blockCell(point)
                res.blockCell(openedPoint)
                res.add(EndTurn())
            } else {
                res.add(EndTurn())
                res.closeCell(openedPoint)
                res.closeCell(point)
            }
            this.openedPoint = null
        } else {
            this.openedPoint = point
        }

        if (cellsRemainder == 0) {
            res.add(Win())
        }

        return res
    }

    private data class Cell(val value: Int, var isOpened: Boolean = false)

    data class Point(val lineN: Int, val columnN: Int)

    open class ClickResult private constructor() {
        class OpenCell(val point: Point, val value: Int): ClickResult()
        class CloseCell(val point: Point): ClickResult()
        class BlockCell(val point: Point): ClickResult()
        class EndTurn: ClickResult()
        class Win: ClickResult()
    }
}
