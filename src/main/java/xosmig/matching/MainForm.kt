package xosmig.matching

import javafx.application.Application
import javafx.application.Platform
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.control.ButtonType
import javafx.scene.layout.GridPane
import javafx.stage.Stage
import xosmig.matching.Game.Point

class MainForm: Application() {

    companion object {
        const val TITLE: String = "Matching"
        const val SIZE: Int = 4
        const val SLEEP_TIME: Long = 350
    }

    private val game = Game(SIZE)
    private val buttons = Array(game.size) { Array(game.size) { Button() } }
    private var disabled = false

    override fun start(primaryStage: Stage) {
        primaryStage.title = TITLE

        val grid = GridPane()

        for ((lineN, line) in buttons.withIndex()) {
            for ((columnN, button) in line.withIndex()) {
                button.prefWidthProperty().bind(grid.widthProperty().divide(SIZE))
                button.prefHeightProperty().bind(grid.heightProperty().divide(SIZE))
                grid.add(button, lineN, columnN)
                button.setOnMouseClicked {
                    if (!disabled) {
                        updateState(game.click(Point(lineN, columnN)))
                    }
                }
            }
        }

        primaryStage.scene = Scene(grid, 200.0, 200.0)
        primaryStage.show()
    }

    private fun updateState(updates: List<Game.ClickResult>) {
        for ((num, update) in updates.withIndex()) {
            when (update) {
                is Game.ClickResult.OpenCell -> {
                    getButton(update.point).text = update.value.toString()
                }
                is Game.ClickResult.CloseCell -> {
                    getButton(update.point).text = ""
                }
                is Game.ClickResult.BlockCell -> {
                    getButton(update.point).disableProperty().value = true
                }
                is Game.ClickResult.EndTurn -> {
                    disabled = true
                    Thread {
                        Thread.sleep(SLEEP_TIME)
                        Platform.runLater {
                            updateState(updates.subList(num + 1, updates.size))
                            disabled = false
                        }
                    }.start()
                    return
                }
                is Game.ClickResult.Win -> {
                    Alert(Alert.AlertType.NONE, "Well done!", ButtonType.CLOSE).showAndWait()
                    Platform.exit()
                }
            }
        }
    }

    private fun getButton(point: Point) = buttons[point.lineN][point.columnN]
}
